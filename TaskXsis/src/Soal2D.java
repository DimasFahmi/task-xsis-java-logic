import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Soal2D {

	public static void main(String[] args) {
		// Variable
		int angka;

		System.out.println("Masukkan Nilai : ");
		Scanner keyboard = new Scanner(System.in);

		angka = keyboard.nextInt();

		int value = 0;

		int original[][] = new int[angka][angka];
		int result[][] = new int[angka][angka];

		for (int i = 0; i < angka;i++) {
			for (int j = 0; j < angka; j++) {
				value = value + 1;
				original[i][j] = value;
			}
		}
		
		for (int i = 0; i < angka;i++) {
			for (int j = 0; j < angka; j++) {
				System.out.print(original[i][j] +  " ");
			}
			System.out.println();
		}
		
		System.out.println();System.out.println();
		
		for (int i = 0; i < angka;i++) {
			for (int j = 0; j < angka; j++) {
				if (j % 2 == 0) {
					System.out.print(original[j][i] +  " ");
				} else {
					System.out.print(original[j][angka - i - 1] +  " ");
				}				
			}
			System.out.println();
		}
	}
}
