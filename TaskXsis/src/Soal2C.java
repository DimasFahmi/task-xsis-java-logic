import java.util.Scanner;

public class Soal2C {

	public static void main(String[] args) {
		// Variable
		int angka;
		
		System.out.println("Masukkan Nilai : ");
        Scanner keyboard = new Scanner(System.in);
        
        int result = 0;
        String jenisOperasi = "C";
        angka = keyboard.nextInt();

        for (int i = 1; i <= angka; i++) {
			for (int j = 1; j <= i; j++) {
				if (result == 1) {
					jenisOperasi = "A";
				} else if (result == angka) {
					jenisOperasi = "B";
				}
				
				if (jenisOperasi.equals("A")) {
					result = result + 1;
				} else if (jenisOperasi.equals("B")) {
					result = result - 1;
				} else {
					result = result + 1;
				}
				
				System.out.print(result);
			}
			System.out.println();
		}

	}

}
