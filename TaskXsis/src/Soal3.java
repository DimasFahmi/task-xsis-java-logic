import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Soal3 {
	public static void main(String[] args) {
		int[] angka =  new int[] {12, 9, 13, 6, 10, 4, 7, 2, 102, 100, 333};
		List<Integer> data = new ArrayList<>(); 
		
		for (int i = 0; i < angka.length; i++) {
			data.add(angka[i]);
		}

		for (int i = angka.length - 1; i >= 0; i--) {
			if (data.get(i) % 3 == 0) {
				data.remove(i);
			}
		}
		Collections.sort(data);
		System.out.println(data);

	}
}
