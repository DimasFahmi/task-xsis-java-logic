import java.util.Scanner;

public class Soal1 {

	public static void main(String[] args) {
		// Variable
		int angka;
		
		System.out.println("Masukkan Nilai : ");
        Scanner keyboard = new Scanner(System.in);
        
        angka = keyboard.nextInt();

        for (int i = 1; i <= angka; i++ ) {
        	if (i % 3 == 0 && i % 4 == 0) {
        		System.out.print(" OKYES");
        	} else if (i % 3 == 0 ) {
        		System.out.print(" OK");
        	} else if (i % 4 == 0 ) {
        		System.out.print(" YES");
        	} else {
        		System.out.print(" " + i);
        	}        	
        }

	}

}
