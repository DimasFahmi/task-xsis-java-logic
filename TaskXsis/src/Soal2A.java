import java.util.Scanner;

public class Soal2A {

	public static void main(String[] args) {
		// Variable
		int angka;

		System.out.println("Masukkan Nilai : ");
		Scanner keyboard = new Scanner(System.in);

		angka = keyboard.nextInt();

		for (int i = 1; i <= angka; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(i);
			}
			System.out.println();
		}

	}

}
