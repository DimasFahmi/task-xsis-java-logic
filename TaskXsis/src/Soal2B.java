import java.util.Scanner;

public class Soal2B {

	public static void main(String[] args) {
		// Variable
		int angka;

		System.out.println("Masukkan Nilai : ");
		Scanner keyboard = new Scanner(System.in);

		angka = keyboard.nextInt();

		for (int i = 1; i <= angka; i++) {
			for (int j = 0; j < i; j++) {
				System.out.print(i - j);
			}
			System.out.println();
		}

	}

}
