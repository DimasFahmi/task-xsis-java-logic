CREATE TABLE Barang (
	KODE VARCHAR ( 11 ) PRIMARY KEY,
	NAMA VARCHAR ( 255 ),
	HARGASATUAN VARCHAR ( 255 )
);

CREATE TABLE Transaksi (
	KODE VARCHAR ( 255 ) PRIMARY KEY,
	TANGGAL DATE,
	KODEPELANGGAN VARCHAR ( 255 ),
	KODEBARANG VARCHAR ( 255 ),
	JUMLAHBARANG DOUBLE PRECISION
);

CREATE TABLE Pelanggan (
	KODE VARCHAR ( 255 ) PRIMARY KEY,
	NAMA VARCHAR ( 255 ),
	ALAMAT VARCHAR ( 255 )
);

INSERT INTO Barang (KODE, NAMA, HARGASATUAN)
VALUES('B1', 'BAJU', 12000),
    ('B2', 'Celana', 10000),
    ('B3', 'SEPATU', 13000);
    
INSERT INTO PELANGGAN (KODE, NAMA, ALAMAT)
VALUES('P1', 'Yogi', 'JAKARTA'),
    ('P2', 'Anggi', 'BANDUNG'),
    ('P3', 'Rahma', 'BANDUNG');
    
INSERT INTO TRANSAKSI (KODE, TANGGAL, KODEPELANGGAN, KODEBARANG, JUMLAHBARANG)
VALUES('TRX001', '2019-10-01', 'P1', 'B1', '3'),
    ('TRX002', '2019-10-02', 'P2', 'B2', '2'),
    ('TRX003', '2019-10-08', 'P2', 'B1', '5'),
	('TRX004', '2019-10-10', 'P1', 'B1', '1'),
    ('TRX005', '2019-10-17', 'P3', 'B2', '2'),
    ('TRX006', '2019-10-17', 'P2', 'B3', '1'),
	('TRX007', '2019-10-18', 'P3', 'B1', '4'),
    ('TRX008', '2019-10-18', 'P2', 'B2', '2');
    
    
    
 -- Soal Nomer 4
 -- A
select * from BARANG where cast(hargasatuan as int) > 10000;
 
 -- B
 select * from PELANGGAN where LOWER(alamat) like '%g%';
 
 --C
select t.kode, t.tanggal, p.nama as namapelanggan, b.nama as namabarang, t.jumlahbarang, 
b.hargasatuan, 
cast(b.hargasatuan as int) * 
cast(t.jumlahbarang  as int) as TOTAL
from transaksi t 
inner join barang b on t.kodebarang = b.kode
inner join pelanggan p on t.kodepelanggan = p.kode
order by p.nama, t.tanggal
;
--D
select p.nama as namapelanggan, sum(cast(t.jumlahbarang  as int)) as jumlah,
sum(cast(b.hargasatuan as int) * 
cast(t.jumlahbarang  as int)) as totalharga
from transaksi t 
inner join barang b on t.kodebarang = b.kode
inner join pelanggan p on t.kodepelanggan = p.kode
group by p.nama
order by totalharga desc
;
 